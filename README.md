# Energy Signature Analyser

This is a toolbox to analyse energy signatures of buildings and compare the signatures of all buildings within an entire building stock.
Energy signatures are simple scatter plots comparing outside air temperature (x-axis) with the building's energy consumption (usually smart meter data resampled to daily mean).
By analysing the shape of such a scatter plot one can estimate the buildings' energy efficiency on the one side,
on the other one can extract characteristics of buildings like the installed heating system or which type of building it is.

## Method
This stack of codes explicitly aims at extracting prevailing building characteristics in a building stock. Therefore,
the shape of each building's energy signature is extracted (using a univariate Support Vector Regression model), and similar shapes clustered (Dynamic Time Warping based). The clustering helps to estimate
the type of each building (apartment, free standing) and its heating system (gas furnace, heat pump, electrict resistance heater).

![](Method.png)

## Overview on codes
The analysis is conducted by calling methods of two classes. One class is dedicated to analyse the energy signature
of each building individually (Python module ES.py) and the second class jointly analyses all buildings in a stock (ES_buildingstock.py).

![](Structure_Code.png)

## Application
The toolbox helps to automatically conduct data-driven retrofit analysis to:
* Find building characteristcs
* Find building specific performance metrics (heating rate, cooling rate)
* Benchmarking performance of buildings with similar characteristics.


## Contact:

Paul Westermann [Energy in Cities group, University of Victoria, Canada](https://energyincities.gitlab.io/website/)

<img src="uvic_logo.jpg" alt="drawing" width="200"/>
