import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from ES import enerSign

from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.metrics import silhouette_samples, silhouette_score

#TSLearn
from tslearn.clustering import TimeSeriesKMeans
from tslearn.clustering import silhouette_score as ts_silhouette_score
from tslearn.utils import to_time_series_dataset

from tslearn.metrics import cdist_gak, cdist_dtw, cdist_soft_dtw, cdist_soft_dtw_normalized, dtw

# DTA i distance
from dtaidistance import clustering
from dtaidistance import dtw
from scipy.cluster import hierarchy

import pickle
import time

class enerSign_bs():
    def __init__(self, df_stock_usage, df_stock_temp):
        self.buildingID = []
        self.energySignatures = []
        self.scores = []
        self.clusters = []
        self.profiles = []
        self.sample_silhuette_scores=[]


        if type(df_stock_temp)==pd.core.series.Series:
            # Case II: All buildings of the stock are in the same city.
            for building,id in enumerate(df_stock_usage.columns):
                self.energySignatures.append(enerSign(df_use=df_stock_usage.iloc[:,building],
                                                 df_temperature=df_stock_temp))
                self.buildingID.append(id)

        elif len(df_stock_usage.columns)==len(df_stock_temp.columns):
            # Case I: All buildings have a temperature time series provided.
            for building,_ in enumerate(df_stock_usage.columns):
                self.energySignatures.append(enerSign(df_use=df_stock_usage.iloc[:,building],
                                                 df_temperature=df_stock_temp.iloc[:,building]))
        else:
            print('Number of weather files do not correspond to the number of buildings and therefore, the location identification is initialized.')
            location_identification()
            pass

    def outlier_detection(self, contamination=.05):
        for ES in self.energySignatures:
            ES.outlier_detection(contamination=contamination)

    def normalize(self, normalizer='z-score', floor_areas=None):
        for ind, ES in enumerate(self.energySignatures):
            if floor_areas is None:
                ES.normalize(normalizer=normalizer)
            else:
                ES.normalize(normalizer=normalizer, floor_area=floor_areas[ind])

    def svr_fit(self, mode='min_cutoff', plot=False):
        for ES in self.energySignatures:
            _, score = ES.svr_fit(mode=mode, plot=plot)
            print(score)
            self.scores.append(score)

        self.get_align_profiles()

        # Figure
        plt.hist(self.scores)
        plt.title(f'R^2_mean: {np.mean(self.scores)}')

    def get_physical_properties(self, metrics = ['bp','cr']):
        for ES in self.energySignatures:
            ES.physical_properties(metrics=metrics)

    def clustering_kmeans(self, n_clusters=5, plot=True, subplots=False, savefig=False):
        model = KMeans(n_clusters=n_clusters).fit(self.profiles)
        self.clusters = model.predict(self.profiles)
        if plot == True:
            plot_clusters(self.profiles, self.clusters, n_clusters, self.temp_range, subplots=subplots, savefig=savefig)

    def clustering_kmeans_dtw(self, n_clusters=5, plot=True, subplots=False, savefig=False):
        as_ts = to_time_series_dataset(self.profiles)
        km = TimeSeriesKMeans(n_clusters=n_clusters, metric="softdtw", metric_params={"gamma_sdtw": .001}, max_iter=30 )
        self.clusters = km.fit_predict(as_ts)
        if plot == True:
            plot_clusters(self.profiles, self.clusters, n_clusters, self.temp_range, subplots=subplots, savefig=savefig)

    def clustering_hierarchical(self, n_clusters=5, plot=True, subplots=False, savefig=False):
        clusters = AgglomerativeClustering(n_clusters=n_clusters, linkage='ward').fit_predict(self.profiles)
        self.clusters = clusters
        if plot == True:
            plot_clusters(self.profiles, self.clusters, n_clusters, self.temp_range, subplots=subplots, savefig=savefig)


    def clustering_hierarchical_dtw(self, n_clusters, plot=True, subplots=False, savefig=False):
        model3 = clustering.LinkageTree(dtw.distance_matrix_fast, {})
        model3.fit(self.profiles)
        self.clusters = hierarchy.cut_tree(model3.linkage, n_clusters=n_clusters).ravel()
        if plot == True:
            plot_clusters(self.profiles, self.clusters, n_clusters, self.temp_range, subplots=subplots, savefig=savefig)

    def elbow_kmeans(self, savefig=False):
        score = np.empty([13])
        for j, i in enumerate(range(2, 15)):
            model = KMeans(n_clusters=i).fit(self.profiles)
            score[j] = model.inertia_

        plt.plot(np.arange(2, 15), score, '-o', color=cm.nipy_spectral(float(0) / 10), linewidth=2)
        plt.plot(6, score[4], 'o', markersize=10, zorder=1, color='red')
        plt.ylabel('Sum of squared errors []')
        plt.xlabel('Number of clusters')
        if savefig==True:
            plt.tight_layout()
            plt.savefig('SSE_plot.eps')

    def plot_silhuettes(self, clusterer='K-means clustering', plot=True):
        # see https://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_silhouette_analysis.html
        range_n_clusters = [2, 3, 4, 5, 6, 7, 8, 9, 10]

        score = np.empty(len(range_n_clusters))
        score_clustermean = np.empty(len(range_n_clusters))
        for j, n_clusters in enumerate(range_n_clusters):
            # Create a subplot with 1 row and 2 columns
            if plot==True:
                fig, (ax1, ax2) = plt.subplots(1, 2)
                fig.set_size_inches(10, 4.5)

                # The 1st subplot is the silhouette plot
                # The silhouette coefficient can range from -1, 1 but in this example all
                # lie within [-0.1, 1]
                ax1.set_xlim([-0.1, 1])
                # The (n_clusters+1)*10 is for inserting blank space between silhouette
                # plots of individual clusters, to demarcate them clearly.
                ax1.set_ylim([0, len(self.profiles) + (n_clusters + 1) * 10])

            # Initialize the clusterer with n_clusters value and a random generator
            # seed of 10 for reproducibility.

            if clusterer == 'Hierarchical clustering':
                self.clustering_hierarchical(n_clusters, plot=False)
                score[j] = silhouette_score(self.profiles, np.array(self.clusters))
                self.sample_silhuette_scores = silhouette_samples(self.profiles, np.array(self.clusters))
            elif clusterer == 'K-means clustering':
                self.clustering_kmeans(n_clusters, plot=False)
                score[j] = silhouette_score(self.profiles, np.array(self.clusters))
                self.sample_silhuette_scores = silhouette_samples(self.profiles, np.array(self.clusters))
            elif clusterer == 'Dtw_kmeans':
                now = time.time()
                self.clustering_kmeans_dtw(n_clusters,plot=False)
                temp = cdist_dtw(self.profiles)
                score[j] = silhouette_score(temp, np.array(self.clusters), metric="precomputed")
                self.sample_silhuette_scores = silhouette_samples(temp, np.array(self.clusters), metric="precomputed")
                print(time.time() - now)
            elif clusterer == 'Dtw_hierarchical':
                now = time.time()
                self.clustering_hierarchical_dtw(n_clusters,plot=False)
                temp = dtw.distance_matrix_fast(self.profiles, compact=False)
                #convert upper traingular to square matrix.
                temp[temp == np.inf] = 0
                temp = temp + np.transpose(temp)
                score[j] = silhouette_score(temp, np.array(self.clusters), metric="precomputed")
                print(time.time()-now)
                self.sample_silhuette_scores = silhouette_samples(temp, np.array(self.clusters), metric="precomputed")
                print(time.time() - now)
            else:
                print('Provide correct clustering')
            # The silhouette_score gives the average value for all the samples.
            # This gives a perspective into the density and separation of the formed
            # clusters

            score_clustermean[j] = np.mean([np.mean(self.sample_silhuette_scores [self.clusters == nc]) for nc in range(n_clusters)])
            print("For n_clusters =", n_clusters,
                  "The average silhouette_score is :", score[j])

            np.save(f'Clusters{n_clusters}_{time.strftime("%m-%d_%H%M%S")}', self.clusters)

            colors = ['r', 'g', 'b', 'y', 'steelblue', 'pink', 'k', 'grey', 'purple', 'r', 'g', 'b', 'y', 'steelblue',
                      'cyan', 'pink', 'k', 'grey', 'purple']

            y_lower = 10
            if plot==True:
                for i in range(n_clusters):
                    # Aggregate the silhouette scores for samples belonging to
                    # cluster i, and sort them
                    ith_cluster_silhouette_values = \
                        self.sample_silhuette_scores[np.array(self.clusters) == i]

                    ith_cluster_silhouette_values.sort()

                    size_cluster_i = ith_cluster_silhouette_values.shape[0]
                    y_upper = y_lower + size_cluster_i

                    color = colors[i]
                    ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)

                    # Label the silhouette plots with their cluster numbers at the middle
                    ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

                    # Compute the new y_lower for next plot
                    y_lower = y_upper + 10  # 10 for the 0 samples

                ax1.set_title("The silhouette plot for the various clusters.")
                ax1.set_xlabel("The silhouette coefficient values")
                ax1.set_ylabel("Cluster label")

            # The vertical line for average silhouette score of all the values
                ax1.axvline(x=score[j], color="red", linestyle="--")


            # ax1.set_yticks([])  # Clear the yaxis labels / ticks
            # ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])


            # Plot clusters
            if plot == True:
                n_points = len(self.profiles[1, :])
                plt.figure(figsize=(20, 10))
                for i in range(n_clusters):
                    profile = self.profiles[self.clusters == i].mean(axis=0)
                    ax2.plot(np.expand_dims(np.arange(0, n_points), axis=1), profile, 'o-', color=colors[i], zorder=8)

                # strings = [
                #    'Cluster ' + str(i) + ' (No. of build.: ' + str(cluster_labels.value_counts()[i]) + ')' for i
                #    in range(n_clusters)]
                # ax2.legend(strings)

        plt.figure()
        plt.plot(range_n_clusters, score, 'k-o')
        plt.ylabel('Silhouette Score')
        plt.xlabel('Number of clusters')
        plt.savefig('Silhouette_scores.eps')

        plt.show()
        return score, score_clustermean

    def get_silhouette_scores(self, clusterer=[]):
        if clusterer == 'Hierarchical clustering':
            self.sample_silhuette_scores = silhouette_samples(self.profiles, np.array(self.clusters))
        elif clusterer == 'K-means clustering':
            self.sample_silhuette_scores = silhouette_samples(self.profiles, np.array(self.clusters))
        elif clusterer == 'Dtw_kmeans':
            temp = cdist_dtw(self.profiles)
            self.sample_silhuette_scores = silhouette_samples(temp, np.array(self.clusters), metric="precomputed")
        elif clusterer == 'Dtw_hierarchical':
            temp = dtw.distance_matrix_fast(self.profiles, compact=False)
            # convert upper traingular to square matrix.
            temp[temp == np.inf] = 0
            temp = temp + np.transpose(temp)
            self.sample_silhuette_scores = silhouette_samples(temp, np.array(self.clusters), metric="precomputed")
        else:
            print('Provide correct clustering')

    def get_align_profiles(self):
        bottom = max([i.profile.Temperature.min() for i in self.energySignatures])
        top = min([i.profile.Temperature.max() for i in self.energySignatures])
        profiles_building = [
            i.profile.Usage_hat[np.logical_and(i.profile.Temperature >= bottom, i.profile.Temperature <= top)].values
            for i in self.energySignatures]
        self.profiles = np.vstack(profiles_building)
        self.temp_range = np.arange(bottom, top+.5, .5).reshape(-1, 1)

    def temp_cut_off(self):
        bottom = max([i.temp.min() for i in self.energySignatures])
        top = min([i.temp.max() for i in self.energySignatures])
        for i in self.energySignatures:
            i.use = i.use[np.logical_and(i.temp >= bottom, i.temp <= top)]
            i.temp = i.temp[np.logical_and(i.temp >= bottom, i.temp <= top)]

    def filter_by_r2(self, t):
        mask=np.array(self.scores) > t
        self.energySignatures = [self.energySignatures[ind] for ind,i in enumerate(mask) if i]
        self.scores = [self.scores[ind] for ind,i in enumerate(mask) if i]
        self.buildingID = [self.buildingID[ind] for ind, i in enumerate(mask) if i]
        if len(self.clusters)==0:
            pass
        else:
            self.clusters = [self.clusters[ind] for ind,i in enumerate(mask) if i]

        self.profiles = self.profiles[mask]
        print(f'Remaining number of buildings: {sum(mask)}')

    def filter(self, mask):
        # mask=np.array(self.scores) > .7
        self.energySignatures = [self.energySignatures[ind] for ind,i in enumerate(mask) if i]
        self.scores = [self.scores[ind] for ind,i in enumerate(mask) if i]
        self.buildingID = [self.buildingID[ind] for ind, i in enumerate(mask) if i]
        if len(self.clusters)==0:
            pass
        else:
            self.clusters = [self.clusters[ind] for ind,i in enumerate(mask) if i]

        if len(self.sample_silhuette_scores)==0:
            pass
        else:
            self.sample_silhuette_scores = [self.sample_silhuette_scores[ind] for ind,i in enumerate(mask) if i]

        self.profiles = self.profiles[mask]
        print(f'Remaining number of buildings: {sum(mask)}')

    def plot_stock(self):
        plt.figure(figsize=(80,60))
        for ind, ES in enumerate(self.energySignatures):
            plt.subplot(np.ceil(len(self.energySignatures) / 10), 10, ind+1)
            plt.plot(ES.temp,ES.use,'x')
        plt.tight_layout()

    def plot_profiles_2D(self):
        from keras.models import Model, load_model
        from keras.layers import Input, Dense
        from keras.callbacks import ModelCheckpoint, TensorBoard
        from keras import regularizers

        nb_epoch = 100
        batch_size = 60
        input_dim = self.profiles.shape[1]  # num of columns, 30
        encoding_dim = 30
        hidden_dim = 2  # i.e. 7
        learning_rate = 1e-7

        train_x = self.profiles[:int(1*len(self.profiles)),:]
        test_x = train_x

        # Pick network architecture
        input_layer = Input(shape=(input_dim,))
        encoder = Dense(encoding_dim)(
            input_layer)
        encoder = Dense(hidden_dim)(encoder)
        decoder = Dense(encoding_dim)(encoder)
        decoder = Dense(input_dim)(decoder)

        # Compile models
        autoencoder = Model(inputs=input_layer, outputs=decoder)
        encoded = Model(inputs=input_layer, outputs = encoder)


        autoencoder.compile(metrics=['accuracy'],
                            loss='mean_squared_error',
                            optimizer='adam')

        cp = ModelCheckpoint(filepath="autoencoder_fraud.h5",
                             save_best_only=True,
                             verbose=0)


        history = autoencoder.fit(train_x, train_x,
                                  epochs=nb_epoch,
                                  batch_size=batch_size,
                                  shuffle=True,
                                  validation_data=(test_x, test_x),
                                  verbose=1,
                                  callbacks=[cp]).history

        test_x_predictions = autoencoder.predict(test_x)
        mse = np.mean(np.power(test_x - test_x_predictions, 2), axis=1)

        self.profiles_encoded = encoded.predict(self.profiles)

        import seaborn as sns
        import matplotlib.cm as cm
        out = self.profiles_encoded
        fig = plt.figure()
        fig.add_subplot(121)
        sns.kdeplot(out[:, 0], out[:, 1], shade=True)
        ax = fig.add_subplot(122)
        color_map = cm.get_cmap('Accent')
        C = np.array([color_map(i) for i in range(max(self.clusters) + 1)])
        ax.scatter(out[:, 0], out[:, 1], color=C[self.clusters])


        return mse


    def save(self, name):
        with open(name, 'wb') as output:
            pickle.dump(self, output)




def location_identification():
    return locations

def SVR_fit():
    return models, building_profiles


def plot_all(type='individual/one plot', option='include model fit'):
    return axis



def dtw_based_clustering():
    from tslearn.clustering import TimeSeriesKMeans
    from tslearn.utils import to_time_series_dataset
    as_ts = to_time_series_dataset(self.profiles)
    km = TimeSeriesKMeans(n_clusters=6, metric="dtw")
    y_pred = km.fit_predict(as_ts)


    for yi in range(3):
        plt.subplot(3, 3, 7 + yi)
        for xx in as_ts[y_pred == yi]:
            plt.plot(xx.ravel(), "k-", alpha=.2)
        plt.plot(km.cluster_centers_[yi].ravel(), "r-")
        plt.xlim(0, as_ts.shape[1])
        plt.ylim(-4, 4)
        if yi == 1:
            plt.title("Soft-DTW $k$-means")


def dtw_hierarchical_clustering():
    model1 = clustering(dtw.dtistance_matrix_fast(self.profiles, window=25, ),{})





def plot_clusters(profiles_building, clusters, n_clusters, temp_range, subplots=False, savefig=False):
    profiles = np.empty([n_clusters, len(profiles_building[0, :])])
    profiles_std = np.empty([n_clusters, len(profiles_building[0, :])])

    colors = ['r', 'g', 'b', 'y', 'steelblue', 'pink', 'k', 'grey', 'purple', 'r', 'g', 'b', 'y', 'steelblue', 'cyan',
              'pink', 'k', 'grey', 'purple']

    if subplots != True:
        fig = plt.figure(figsize=(20, 10))
    for i in range(n_clusters):
        if subplots == True:
            fig = plt.figure(num=1, figsize=(1.6 * n_clusters, 5))
            plt.subplot(2, np.ceil(n_clusters / 2), i + 1)

        else:
            pass
        profiles[i, :] = profiles_building[clusters == i].mean(axis=0)
        profiles_std[i, :] = profiles_building[clusters == i].std(axis=0)
        plt.plot(temp_range, profiles[i, :], '-', linewidth=4, zorder=8)

    strings = ['Cluster ' + str(i) + ' (No. of build.: ' + str(np.sum(clusters == i)) + ')' for i in range(n_clusters)]
    # plt.legend(strings)

    if subplots != True:
        plt.figure(figsize=(20, 10))
    for i in range(n_clusters):
        if subplots == True:
            plt.figure(num=1, figsize=(1.6 * n_clusters, 5))
            plt.subplot(2, np.ceil(n_clusters / 2), i + 1)
        else:
            pass
        for prof in profiles_building[clusters == i]:
            plt.plot(temp_range,
                     prof, color=colors[i],
                     linestyle='--', linewidth=.8, alpha=.9, zorder=5)
            plt.ylim([-1, 2.5])

    fig.text(0.5, 0.01, u'Outside air temperature [℃]', ha='center')
    fig.text(-0.01, 0.5, 'Stand. electricity consumption', va='center', rotation='vertical')
    plt.tight_layout()
    if savefig == True:
        plt.savefig('cluster_plot.eps', bbox_inches='tight')

    if subplots != True:
        fig = plt.figure(figsize=(20, 10))
    for i in range(n_clusters):
        if subplots == True:
            fig = plt.figure(num=2, figsize=(1.6 * n_clusters, 5))
            plt.subplot(2, np.ceil(n_clusters / 2), i + 1)
        else:
            pass
        plt.plot(temp_range[:-1], np.diff(profiles[i, :]), 'o-', color=colors[i], zorder=8)
    fig.text(0.5, 0.01, u'Outside air temperature [℃]', ha='center')
    fig.text(-0.01, 0.5, 'Heating/cooling rate [kWh/℃]', va='center', rotation='vertical')
    plt.tight_layout()