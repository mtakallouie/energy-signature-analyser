import numpy as np
import pandas as pd
# For normalization
from sklearn.preprocessing import StandardScaler

# For linear model fit and base load identification
from GPyOpt.methods import BayesianOptimization
import pwlf

# For outlier detection
from sklearn.neighbors import LocalOutlierFactor
from sklearn.covariance import EmpiricalCovariance

# Generally for plotting
import matplotlib.pyplot as plt

# For ML model fitting
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import r2_score

# Others
import time

#####################################################################

class enerSign():
    def __init__(self, df_use, df_temperature):
        temp = pd.concat([df_use, df_temperature],axis=1).dropna()
        self.use = temp.iloc[:,0].values
        self.temp = temp.iloc[:,1].values
        self.use_original_data = temp.iloc[:,0].values
        self.temp_original_data = temp.iloc[:, 1].values
        self.profile = []

    def normalize(self, normalizer='z_score', floor_area=None):
        if normalizer == 'z_score':
            stdzer = StandardScaler()
            stdzer.fit(self.use.reshape(-1, 1))
            self.use = stdzer.transform(self.use.reshape(-1, 1))
        elif normalizer == 'ES_norm_I':
            bl = self.get_base_load()
            self.use = self.use / bl
        elif normalizer == 'floor_area':
            if (np.std(self.use) - 1) < 0.1:
                if len(self.use_original_data)>len(self.use):
                    print('undo previous outlier detection')
                    self.use = self.use_original_data * 1.0
                    self.temp = self.temp_original_data * 1.0
                    self.outlier_detection()
                else:
                    self.use = self.use_original_data * 1.0
                    self.temp = self.temp_original_data * 1.0
            self.use = self.use/floor_area

        return self.use

    def get_base_load(self):
        _, my_pwlf = self.multiple_linear_regression()
        return my_pwlf.predict(my_pwlf.x_data).min()

    def multiple_linear_regression(self, plot=False):
        # initialize piecewise linear fit with your x and y data

        my_pwlf = pwlf.PiecewiseLinFit(self.temp, self.use)

        # define your objective function


        def my_obj(x):
            # define some penalty parameter l
            # you'll have to arbitrarily pick this
            # it depends upon the noise in your data,
            # and the value of your sum of square of residuals
            l = self.use.mean() * .05
            f = np.zeros(x.shape[0])
            for i, j in enumerate(x):
                my_pwlf.fit(j[0])
                f[i] = my_pwlf.ssr + (l * j[0])
            return f

        # define the lower and upper bound for the number of line segments
        bounds = [{'name': 'var_1', 'type': 'discrete',
                   'domain': np.arange(2, 5)}]

        np.random.seed(12121)

        myBopt = BayesianOptimization(my_obj, domain=bounds, model_type='GP',
                                      initial_design_numdata=10,
                                      initial_design_type='latin',
                                      exact_feval=True, verbosity=True,
                                      verbosity_model=False)
        max_iter = 30

        # perform the bayesian optimization to find the optimum number
        # of line segments
        myBopt.run_optimization(max_iter=max_iter, verbosity=True)

        print('\n \n Opt found \n')
        print('Optimum number of line segments:', myBopt.x_opt)
        print('Function value:', myBopt.fx_opt)
        # myBopt.plot_acquisition()


        # perform the fit for the optimum
        my_pwlf.fit(myBopt.x_opt)
        # predict for the determined points
        xHat = np.linspace(min(self.temp), max(self.temp), num=10000)
        yHat = my_pwlf.predict(xHat)

        # plot the results
        if plot==True:
            myBopt.plot_convergence()
            plt.figure()
            plt.plot(self.temp, self.use, 'o')
            plt.plot(xHat, yHat, '-')
            plt.show()

        return myBopt, my_pwlf #return myBopt, piecewise linear model

    def svr_fit(self, mode = 'min_cutoff', t_min: int=1, t_max: int=32, plot=False):
        """ Fits a support vector regression model using a grid search cross-validation;
        returns SVR estimator, associatated R2 score"""
        outputs = self.use.reshape(-1, 1)
        inputs = self.temp.reshape(outputs.shape)

        now = time.time()
        #svr = GridSearchCV(SVR(kernel='rbf', epsilon=.3), cv=5, param_grid={"C": [10e2],
        #                                                        "gamma": np.logspace(-5, -1.7, 8)})
        svr = GridSearchCV(SVR(kernel='rbf', epsilon=.3), cv=5, param_grid={"C": [10e0, 10e1],
                                                                "gamma": np.logspace(-2, -2, 8)})
        svr.fit(inputs, outputs.reshape(-1, 1).ravel())

        #print(f'Time to fit the model:{time.time() - now}')

        model = svr.best_estimator_
        score = r2_score(outputs, svr.predict(inputs))
        if mode =='min_cutoff':
            x_hat = np.arange(np.floor(inputs.min()), np.ceil(inputs.max()), .5).reshape(-1, 1)
        elif mode == 'fix_cutoff':
            x_hat = np.arange(t_min, t_max, .5).reshape(-1, 1)
        else:
            raise Exception('Provide correct fitting mode.')
        self.profile = pd.DataFrame(np.transpose([x_hat.ravel(), model.predict(x_hat)]),columns=['Temperature', 'Usage_hat'])

        if plot == True:
            plt.figure()
            plt.plot(inputs, outputs, 'o')
            plt.plot(x_hat, self.profile.Usage_hat, '-o')
            plt.ylabel('Standardized Elec. Consumption []')
            plt.xlabel(u'Outside air temperature [°C]')
            #plt.title(str(score))

        return model, score

    def outlier_detection(self, n_days=5, contamination=.01):
        y_pred = LocalOutlierFactor(n_neighbors=n_days, contamination=contamination).fit_predict(np.vstack((self.temp, self.use)).T)
        self.use = self.use[y_pred == 1]
        self.temp = self.temp[y_pred == 1]
        return y_pred

    def physical_properties(self, metrics = ['bp','cr']):
        try:
            _, linear_model = self.multiple_linear_regression()

            breakpoints = linear_model.fit_breaks
            slopes = linear_model.slopes
            print(breakpoints)
            try:
                if any("bp" in s for s in metrics):
                    mask = (breakpoints > 12) & (breakpoints < 25)
                    # Last and first entry no balance point candidates
                    mask[-1]= False
                    mask[0] = False
                    slopes_temp = slopes[mask[1:]]
                    for n,bp in enumerate(breakpoints[mask]):
                        old = bp
                        if slopes_temp[n]>3:
                            pass
                        else:
                            self.bp=bp


                    print(f'Balance point is: {self.bp}')
                if any("cr" in s for s in metrics):
                    mask = (breakpoints > 12)
                    slopes_temp = slopes[mask[1:]]
                    self.cr = max(slopes_temp)
                    print(f'Cooling rate is: {self.cr}')
                if any("hr" in s for s in metrics):
                    mask = (breakpoints < 15)
                    slopes_temp = slopes[mask[1:]]
                    slopes_temp = slopes_temp[slopes_temp<0.5]
                    self.hr = min(slopes_temp)
                    print(f'Heating rate is: {self.hr}')

            except:
                print('No balance point, cooling or heating rate found.')
                if any("bp" in s for s in metrics):
                    self.bp = np.nan
                if any("cr" in s for s in metrics):
                    self.cr = np.nan
                if any("hr" in s for s in metrics):
                    self.hr = np.nan
        except:
            print('No linear model could be fitted.')

    def plot(self):
        plt.plot(self.temp, self.use, 'o')
        plt.ylabel('Electricity consumption [kWh]')
        plt.xlabel(u'Outside air temperature [°C]')




def running_mean_temp(temp, half_live):
    # fill in missing temperature values
    temp.fillna(temp.rolling(3, min_periods=1).mean(), inplace=True)

    alpha = 1 - 0.69 / half_live
    print(f'Alpha value is: {alpha}')
    init_rmtemp = 0
    cut_off = int(10 * half_live)
    print(cut_off)
    # initialize dataframe
    df_weath = pd.DataFrame(index=temp.index, columns=['temp', 'rm_temp'])
    df_weath['temp'] = temp

    # initial running_mean_temp
    for i in range(int(10 * half_live)):
        init_rmtemp += alpha ** i * temp[:cut_off].values[cut_off - 1 - i]
    df_weath.iloc[cut_off, 1] = init_rmtemp * (1 - alpha)

    # fill the rest
    for i in range(1, len(df_weath.index[cut_off + 1:])):
        if i % 4000 == 0:
            print(i)
        df_weath.iloc[cut_off + i, 1] = (1 - alpha) * df_weath.iloc[cut_off + i - 1, 0] + \
                                        alpha * df_weath.iloc[cut_off + i - 1, 1]

    return df_weath


def outlier_detection_old(df_smartmeter, t=0.05):
    """ This function removes the outliers in the ES assuming the data to be iid (...)."""

    emp_cov = EmpiricalCovariance().fit(df_smartmeter.values)
    mahal_score = emp_cov.mahalanobis(df_smartmeter.values - np.mean(df_smartmeter.values))

    plt.hist(mahal_score)
    df_no_outl = df_smartmeter.loc[mahal_score>t,:]
    return df_no_outl, mahal_score

def piecewise_linear_fit_np(x_vec, y_vec, model_type='m1' ):
    from scipy import optimize
    if model_type=='m1':
        def type1(x, beta0, beta1, a0, a1, b0):
            return np.piecewise(x, [x < beta0, (x >= beta0) & (x < beta1), x >= beta1],[lambda x: a0*x+b0, a0*beta0 + b0,
                                                                            lambda x: a0*beta0 + b0 + a1*x])

        p,_ = optimize.curve_fit(type1, x_vec, y_vec)
        model = lambda x: type1(x, p)
    return model



















